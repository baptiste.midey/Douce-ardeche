    window.ctoutvert = {
        id: 3542,
        lang: 'auto',
        url: 'https://bookingpremium.secureholiday.net/widgets/', 
    };

    (function (w, d, s, ctv, r, js, fjs) {
        r=new XMLHttpRequest();r.open('GET',w[ctv].url+'js/src.json');
        r.responseType='json';r.json=true;r.send();
        r.onload=function(){w[ctv].src=r.responseType=='json'?r.response:JSON.parse(r.response);
        js.src=w[ctv].src[0];fjs.parentNode.insertBefore(js, fjs);}
        js=d.createElement(s),fjs=d.getElementsByTagName(s)[0];
        js.id='ctvwidget';js.async=1;
    }(window, document, 'script', 'ctoutvert'));