$(document).ready(function () {
    $(".navbar-nav .dropdown").hover(            
        function() {
            $('.dropdown-menu', this).not('.in .dropdown-menu').stop( true, true ).slideDown(300);
            $(this).toggleClass('open');        
        },
        function() {
            $('.dropdown-menu', this).not('.in .dropdown-menu').stop( true, true ).slideUp(200);
            $(this).toggleClass('open');       
        }
    );
    $('.navbar-nav .dropdown').on('show.bs.dropdown', function(e){
      $(this).find('.dropdown-menu').first().stop(true, true).slideDown(300);
    });
    $('.navbar-nav .dropdown').on('hide.bs.dropdown', function(e){
      $(this).find('.dropdown-menu').first().stop(true, true).slideUp(200);
    });
    $('.navbar-collapse').on('show.bs.collapse', function(e){
      $(this).find('.navbar-collapse').first().stop(true, true).slideDown(300);
    });
    
    $('.navbar-collapse').on('hide.bs.collapse', function(e){
      $(this).find('.navbar-collapse').first().stop(true, true).slideUp(200);
    });
});